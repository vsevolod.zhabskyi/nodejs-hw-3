import React, {useState} from 'react';
import moment from 'moment';
import {Button} from "react-bootstrap";
import CreateUpdateTruckModal from "./CreateUpdateTruckModal";

function TruckItem({truck, onCreate, onAssign, onDelete}) {
  const [showEdit, setShowEdit] = useState(false);

  const handleAssign = () => {
    onAssign(truck._id)
  }

  const handleDelete = () => {
    onDelete(truck._id)
  }

  return (
    <tr>
      <td>{truck.type}</td>
      <td>{moment(truck.created_date).fromNow()}</td>
      <td>{truck.status}</td>
      <td>{truck.assigned_to ? "YES" : "NO"}</td>
      {!truck.assigned_to && (<td>
        <Button className="m-1" onClick={() => setShowEdit(true)}>Edit</Button>
        <CreateUpdateTruckModal onCreate={onCreate} show={showEdit} onHide={() => setShowEdit(false)} truck={truck}/>
        <Button onClick={handleDelete} className="btn-danger">Delete</Button>
        <Button className="m-1" onClick={handleAssign}>Assign</Button>
      </td>)}
    </tr>
  );
}

export default TruckItem;
