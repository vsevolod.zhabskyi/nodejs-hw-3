import { observer } from 'mobx-react-lite';
import React, { useContext } from 'react';
import { Nav, Navbar, Container, Button } from 'react-bootstrap';
import {NavLink, useNavigate} from 'react-router-dom';
import { Context } from '../index';
import {LOADS_ROUTE, LOGIN_ROUTE, TRUCKS_ROUTE} from '../utils/consts';
import {PROFILE_ROUTE} from '../utils/consts';

const NavBar = observer(() => {
  const {user} = useContext(Context)
  const navigate = useNavigate();

  const logOut = () => {
    user.setUser(null)
    user.setIsAuth(false)
    localStorage.removeItem('token')
    navigate(LOGIN_ROUTE)
  }

  return (
    <div>
      <Navbar bg="light" variant="light" style={{height: 70}}>
        <Container>
          <div>
            <NavLink style={{color: 'black', fontSize: '26px', textDecoration: 'none', marginRight: '20px'}} to={LOADS_ROUTE}>Loads</NavLink>
            <NavLink style={{color: 'black', fontSize: '26px', textDecoration: 'none', marginRight: '20px'}} to={PROFILE_ROUTE}>Profile</NavLink>
            {user.isDriver && (<>
              <NavLink style={{color: 'black', fontSize: '26px', textDecoration: 'none', marginRight: '20px'}} to={TRUCKS_ROUTE}>Trucks</NavLink>
            </>)}
          </div>

          <Nav className="ml-auto">
            <Button
              variant='outline-dark'
              onClick={() => logOut()}
              className="mx-3"
            >
              Log out
            </Button>
          </Nav>
        </Container>
      </Navbar>
    </div>
  );
})

export default NavBar;
