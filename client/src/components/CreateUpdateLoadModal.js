import React, {useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap"

const CreateUpdateLoadModal = ({show, onHide, onCreate, load}) => {
  const [name, setName] = useState(load?.name || '')
  const [pickupAddress, setPickupAddress] = useState(load?.pickup_address || '')
  const [deliveryAddress, setDeliveryAddress] = useState(load?.delivery_address || '')
  const [payload, setPayload] = useState(load?.payload || 0)
  const [width, setWidth] = useState(load?.dimensions.width || 0)
  const [length, setLength] = useState(load?.dimensions.length || 0)
  const [height, setHeight] = useState(load?.dimensions.height || 0)

  const handleCreate = () => {
    try {
      const newLoad = {
        name: name,
        pickup_address: pickupAddress,
        delivery_address: deliveryAddress,
        payload: +payload,
        dimensions: {
          width: +width,
          length: +length,
          height: +height
        }
      }

      onCreate(load?._id, newLoad)

      onHide()

    } catch (e) {
      alert(e)
    }
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Create new load
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Label>Load name</Form.Label>
          <Form.Control
            placeholder={'Load name'}
            value={name}
            onChange={(e) => setName(e.target.value)}
          />

          <Form.Label className="mt-2">Pickup address</Form.Label>
          <Form.Control
            placeholder={'Pickup address'}
            value={pickupAddress}
            onChange={(e) => setPickupAddress(e.target.value)}
          />

          <Form.Label className="mt-2">Delivery address</Form.Label>
          <Form.Control
            placeholder={'Delivery address'}
            value={deliveryAddress}
            onChange={(e) => setDeliveryAddress(e.target.value)}
          />

          <Form.Label className="mt-2">Payload</Form.Label>
          <Form.Control
            placeholder={'Payload'}
            value={payload}
            type="number"
            onChange={(e) => setPayload(e.target.value)}
          />

          <h6 className="mt-2">Dimensions</h6>
          <Form.Label>Width</Form.Label>
          <Form.Control
            placeholder={'Width'}
            value={width}
            type="number"
            onChange={(e) => setWidth(e.target.value)}
          />

          <Form.Label className="mt-2">Length</Form.Label>
          <Form.Control
            placeholder={'Length'}
            value={length}
            type="number"
            onChange={(e) => setLength(e.target.value)}
          />

          <Form.Label className="mt-2">Height</Form.Label>
          <Form.Control
            placeholder={'Height'}
            value={height}
            type="number"
            onChange={(e) => setHeight(e.target.value)}
          />
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant={'outline-cancel'} onClick={() => onHide()}>Cancel</Button>
        <Button variant={'outline-dark'} onClick={handleCreate}>Confirm</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default CreateUpdateLoadModal;
