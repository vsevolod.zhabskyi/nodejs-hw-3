import React, {useContext} from 'react';
import {Route, Routes} from 'react-router-dom';
import {Context} from '../index';
import {observer} from 'mobx-react-lite';
import Layout from "./Layout";
import {authRoutes, driverRoutes, publicRoutes} from "../routes";

const AppRouter = observer(() => {

  const {user} = useContext(Context)

  return (
    <Routes>
      {user.isAuth && (
        <Route path="/" element={<Layout/>}>
          {authRoutes.map(({path, element}) =>
            <Route key={path} path={path} element={element}/>
          )}

          {user.isDriver && (<>
            {driverRoutes.map(({path, element}) =>
              <Route key={path} path={path} element={element}/>
            )}
          </>)}
        </Route>
      )}

      {publicRoutes.map(({path, element}) =>
        <Route key={path} path={path} element={element}/>
      )}
    </Routes>
  );
})

export default AppRouter;
