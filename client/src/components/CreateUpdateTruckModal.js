import React, {useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap"

const CreateUpdateLoadModal = ({show, onHide, onCreate, truck}) => {
  const [type, setType] = useState(truck?.type || '')

  const handleCreate = () => {
    try {
      const newTruck = {
        type: type
      }

      onCreate(truck?._id, newTruck)

      onHide()
    } catch (e) {
      alert(e)
    }
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Create new load
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Label>Truck type</Form.Label>
          <Form.Check
            label="SPRINTER"
            name="role"
            type="radio"
            value="SPRINTER"
            onChange={e => setType(e.target.value)}
          />
          <Form.Check
            label="SMALL STRAIGHT"
            name="role"
            type="radio"
            value="SMALL STRAIGHT"
            onChange={e => setType(e.target.value)}
            className="mb-3"
          />
          <Form.Check
            label="LARGE STRAIGHT"
            name="role"
            type="radio"
            value="LARGE STRAIGHT"
            onChange={e => setType(e.target.value)}
            className="mb-3"
          />
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant={'outline-cancel'} onClick={() => onHide()}>Cancel</Button>
        <Button variant={'outline-dark'} onClick={handleCreate}>Confirm</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default CreateUpdateLoadModal;
