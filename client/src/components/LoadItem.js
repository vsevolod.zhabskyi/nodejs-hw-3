import React, {useContext, useState} from 'react';
import moment from 'moment';
import {Button} from "react-bootstrap";
import CreateUpdateLoadModal from "./CreateUpdateLoadModal";
import {LOAD_STATUS_ASSIGNED, LOAD_STATUS_NEW} from "../utils/consts";
import {Context} from "../index";

const LoadItem = ({load, onCreate, onIterateState, onDelete, onPost}) => {
  const [showEdit, setShowEdit] = useState(false);
  const {user} = useContext(Context)

  const handleDelete = () => {
    onDelete(load._id)
  }

  const handlePost = () => {
    onPost(load._id)
  }

  return (
    <tr>
      <td>{load.name}</td>
      <td>{moment(load.created_date).fromNow()}</td>
      <td>{load.pickup_address}</td>
      <td>{load.delivery_address}</td>
      <td>{load.state}</td>
      <td>{load.status}</td>
        {user.isShipper
          ? <>
          {load.status === LOAD_STATUS_NEW && <td>
              <Button className="m-1" onClick={() => setShowEdit(true)}>Edit</Button>
              <CreateUpdateLoadModal onCreate={onCreate} show={showEdit} onHide={() => setShowEdit(false)} load={load}/>
              <Button onClick={handleDelete} className="btn-danger m-1">Delete</Button>
              <Button onClick={handlePost} className="btn-success m-1">Post</Button>
            </td>}
          </>
          :
          <>
            {load.status === LOAD_STATUS_ASSIGNED && <td>
              <Button onClick={onIterateState}>Next state</Button>
            </td>}
          </>
        }
    </tr>
  );
}

export default LoadItem;
