import {LOADS_ROUTE, LOGIN_ROUTE, PROFILE_ROUTE, REGISTRATION_ROUTE, TRUCKS_ROUTE} from "./utils/consts"
import LoginRegister from "./pages/LoginRegister"
import Profile from "./pages/Profile";
import Loads from "./pages/Loads";
import Trucks from "./pages/Trucks";

export const publicRoutes = [
	{
		path: LOGIN_ROUTE,
		element: <LoginRegister/>
	},	{
		path: REGISTRATION_ROUTE,
		element: <LoginRegister/>
	},
]

export const authRoutes = [
	{
		path: LOADS_ROUTE,
		element: <Loads/>
	},
	{
		path: PROFILE_ROUTE,
		element: <Profile/>
	},
]

export const driverRoutes = [
	{
		path: TRUCKS_ROUTE,
		element: <Trucks/>
	}
]
