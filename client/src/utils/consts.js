export const REACT_APP_API_URL='http://localhost:8080/'

export const LOGIN_ROUTE = '/login'
export const REGISTRATION_ROUTE = '/registration'
export const LOADS_ROUTE = '/loads'
export const PROFILE_ROUTE = '/profile'
export const TRUCKS_ROUTE = '/trucks'

export const SHIPPER_ROLE = 'SHIPPER'
export const DRIVER_ROLE = 'DRIVER'

export const LOAD_STATUS_NEW = 'NEW'
export const LOAD_STATUS_ASSIGNED = 'ASSIGNED'
