import './App.css'
import {BrowserRouter} from 'react-router-dom';
import NavBar from './components/NavBar';
import AppRouter from './components/AppRouter';
import {check} from "./API/authAPI";
import {observer} from "mobx-react-lite";
import {useContext, useEffect} from "react";
import {Context} from "./index";

const App = observer(() => {
  const {user} = useContext(Context)

  useEffect(() => {
    check().then(data => {
      user.setUser(data)
      user.setIsAuth(true)
      user.setRole(data.role)
    })
  }, [])

  return (
    <>
      <BrowserRouter>
        <AppRouter/>
      </BrowserRouter>
    </>
  );
})

export default App;
