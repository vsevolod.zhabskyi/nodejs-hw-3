import {makeAutoObservable} from 'mobx'
import {DRIVER_ROLE, SHIPPER_ROLE} from "../utils/consts";

export default class UserStore {
  constructor() {
    this._isAuth = false
    this._role = null
    this._user = {}
    this._isShipper = false
    this._isDriver = false
    makeAutoObservable(this)
  }

  setIsAuth(bool) {
    this._isAuth = bool
  }

  setRole(role) {
    this._role = role
    switch (role) {
      case SHIPPER_ROLE:
        this._isShipper = true;
        this._isDriver = false;
        break;
      case DRIVER_ROLE:
        this._isDriver = true;
        this._isShipper = false;
        break;
    }
  }

  setUser(user) {
    this._user = user
  }

  get isAuth() {
    return this._isAuth
  }

  get role() {
    return this._role
  }

  get isShipper() {
    return this._isShipper
  }

  get isDriver() {
    return this._isDriver
  }

  get user() {
    return this._user
  }
}
