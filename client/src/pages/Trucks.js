import React, {useEffect, useState} from 'react';
import {Button, Table, Container} from "react-bootstrap";
import {observer} from "mobx-react-lite";
import {assignTruck, createTruck, deleteTruck, fetchTrucks, updateTruck} from "../API/truckAPI";
import CreateUpdateTruckModal from "../components/CreateUpdateTruckModal";
import TruckItem from "../components/TruckItem";

const Trucks = observer(() => {
  const [trucks, setTrucks] = useState([]);
  const [isLoading, setIsLoading] = useState(true)

  const [showCreateModal, setShowCreateModal] = useState(false)

  const handleCreateTruck = async (id, truck) => {
    try {
      await createTruck(truck)
        .then(() => fetchTrucks()
        .then((data) => setTrucks(data)))
    } catch (e) {
      alert(e)
    }
  }

  const handleUpdateTruck = async (id, truck) => {
    await updateTruck(id, truck)
      .then(() => fetchTrucks()
      .then((data) => setTrucks(data)))
      .catch(e => alert(e))
  }

  const handleAssign = async (id) => {
    await assignTruck(id)
      .then(() => fetchTrucks())
      .then((data) => setTrucks(data))
  }

  const handleDelete = async (id) => {
    await deleteTruck(id)
      .then(() => fetchTrucks())
      .then((data) => setTrucks(data))
  }

  useEffect(() => {
    fetchTrucks().then(data => {
      setTrucks(data)
      setIsLoading(false)
    })
  }, [])

  return (
    <Container
      className="d-flex flex-column align-items-center pl-5 pr-5"
      style={{height: window.innerHeight - 54}}
    >
      <h1 className="mt-2 mb-5">Trucks</h1>

      <Button
        className="align-self-start mb-4"
        onClick={() => setShowCreateModal(true)}
      >Create truck</Button>
      <CreateUpdateTruckModal onCreate={handleCreateTruck} show={showCreateModal} onHide={() => setShowCreateModal(false)}/>

      <div className="">
        {!isLoading && (
          <Table striped bordered hover>
            <thead>
            <tr>
              <th>Truck type</th>
              <th>Created date</th>
              <th>Status</th>
              <th>Is assigned</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
            {trucks.map(truck =>
              <TruckItem
                key={truck._id}
                truck={truck}
                onCreate={handleUpdateTruck}
                onAssign={handleAssign}
                onDelete={handleDelete}
              />
            )}
            </tbody>
          </Table>
        )}
      </div>
    </Container>
  );
})

export default Trucks;
