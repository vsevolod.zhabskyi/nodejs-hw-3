import React, {useContext, useEffect, useState} from 'react';
import {createLoad, deleteLoad, fetchLoads, iterateState, postLoad, updateLoad} from "../API/loadsAPI";
import {Button, Table, Container} from "react-bootstrap";
import LoadItem from "../components/LoadItem";
import {Context} from "../index";
import {observer} from "mobx-react-lite";
import CreateUpdateLoadModal from "../components/CreateUpdateLoadModal";

const Loads = observer(() => {
  const [loads, setLoads] = useState([]);
  const [isLoading, setIsLoading] = useState(true)

  const {user} = useContext(Context)

  const [showCreateModal, setShowCreateModal] = useState(false)

  const handleCreateLoad = async (id, load) => {
    try {
      await createLoad(load).then(() => {
      }).then(() => fetchLoads()
        .then((data) => setLoads(data)))
    } catch (e) {
      alert(e)
    }
  }

  const handleUpdateLoad = async (id, load) => {
    try {
      await updateLoad(id, load)
        .then(() => fetchLoads()
        .then((data) => setLoads(data)))
    } catch (e) {
      alert(e)
    }
  }

  const handleIterateState = async () => {
    await iterateState()
      .then(() => fetchLoads())
      .then((data) => setLoads(data))
  }

  const handleDelete = async (id) => {
    await deleteLoad(id)
      .then(() => fetchLoads())
      .then((data) => setLoads(data))
  }

  const handlePost = async (id) => {
    await postLoad(id)
      .then(() => fetchLoads())
      .then((data) => setLoads(data))
  }

  useEffect(() => {
    fetchLoads().then(data => {
      setLoads(data)
      setIsLoading(false)
    })
  }, [])

  return (
    <Container
      className="d-flex flex-column align-items-center pl-5 pr-5"
      style={{height: window.innerHeight - 54}}
    >
      <h1 className="mt-2 mb-5">Loads</h1>
      {user.isShipper && <>
        <Button
          className="align-self-start mb-4"
          onClick={() => setShowCreateModal(true)}
        >Create load</Button>
        <CreateUpdateLoadModal onCreate={handleCreateLoad} show={showCreateModal} onHide={() => setShowCreateModal(false)}/>
      </>}
      <div className="">
        {!isLoading && (
          <Table striped bordered hover>
            <thead>
            <tr>
              <th>Load name</th>
              <th>Created date</th>
              <th>Pick-up address</th>
              <th>Delivery address</th>
              <th>State</th>
              <th>Status</th>
            </tr>
            </thead>
            <tbody>
            {loads.map(load =>
              <LoadItem
                key={load._id}
                load={load}
                onCreate={handleUpdateLoad}
                onIterateState={handleIterateState}
                onDelete={handleDelete}
                onPost={handlePost}
              />
            )}
            </tbody>
          </Table>
        )}
        {loads.length === 0 &&
          <p className="text-center">You have no {user.isShipper ? "created" : "assigned"} loads yet.</p>
        }

      </div>
    </Container>
  );
})

export default Loads;
