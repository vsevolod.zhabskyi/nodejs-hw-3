import jwt_decode from "jwt-decode";
import {$authHost, $host} from './index';

export const registration = async (email, password, role) => {
  await $host.post('api/auth/register', {email, password, role})

  const {data} = await $host.post('api/auth/login', {email, password})

  localStorage.setItem('token', data.jwt_token)

  return jwt_decode(data.jwt_token)
}

export const login = async (email, password) => {
  const {data} = await $host.post('api/auth/login', {email, password})

  localStorage.setItem('token', data.jwt_token)

  return jwt_decode(data.jwt_token)
}

export const check = async () => {
  const {data} = await $authHost.get('api/auth/check')

  localStorage.setItem('token', data.jwt_token)

  return jwt_decode(data.jwt_token)
}
