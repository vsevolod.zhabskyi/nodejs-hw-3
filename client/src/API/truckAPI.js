import {$authHost} from "./index";

export const fetchTrucks = async () => {
  const {data} = await $authHost.get('api/trucks');
  return data.trucks;
}

export const createTruck = async (truck) => {
  await $authHost.post('api/trucks', truck);
}

export const updateTruck = async (id, truck) => {
  await $authHost.put(`api/trucks/${id}`, truck);
}

export const assignTruck = async (id) => {
  await $authHost.post(`api/trucks/${id}/assign`);
}

export const deleteTruck = async (id) => {
  await $authHost.delete(`api/trucks/${id}`);
}

