import {$authHost} from "./index";

export const fetchLoads = async () => {
  const {data} = await $authHost.get('api/loads');
  return data.loads;
}

export const createLoad = async (load) => {
  await $authHost.post('api/loads', load);
}

export const updateLoad = async (id, load) => {
  await $authHost.put(`api/loads/${id}`, load);
}

export const iterateState = async () => {
  await $authHost.patch('/api/loads/active/state')
}

export const deleteLoad = async (id) => {
  await $authHost.delete(`api/loads/${id}`);
}

export const postLoad = async (id) => {
  await $authHost.post(`api/loads/${id}/post`);
}
