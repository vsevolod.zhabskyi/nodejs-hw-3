import {$authHost} from './index';

export const fetchUser = async () => {
  const {data} = await $authHost.get('api/users/me/')
  return data.user
}

export const deleteUser = async () => {
  await $authHost.delete('api/users/me/')
}

export const updatePassword = async (oldPassword, newPassword) => {
  await $authHost.patch('api/users/me/', {oldPassword, newPassword})
}

export const uploadPhoto = async (file) => {
  const formData = new FormData();
  formData.append("img", file);
  await $authHost.patch('api/users/me/upload_photo', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}
