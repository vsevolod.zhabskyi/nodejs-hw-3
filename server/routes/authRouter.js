const express = require('express');
const router = express();
const authController = require('../controllers/authController');
const loginValidation = require('../validations/loginValidation');
const registrationValidation = require('../validations/registrationValidation');
const checkAuth = require('../middleware/checkAuthMiddleware');

router.post('/register', registrationValidation, authController.registration);
router.post('/login', loginValidation, authController.login);
router.get('/check', checkAuth, authController.check);

module.exports = router;
