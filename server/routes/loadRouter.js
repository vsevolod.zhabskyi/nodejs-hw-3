const express = require('express');
const router = express();
const loadController = require('../controllers/loadController');
const checkShipper = require('../middleware/checkShipperRoleMiddleware');
const checkDriver = require('../middleware/checkDriverRoleMiddleware');
const validateLoad = require('../validations/loadValidation');
const validateId = require('../validations/idValidation');
const validateGetLoadsQuery = require('../validations/getLoadsQueryValidation');

router.post('/', checkShipper, validateLoad, loadController.create);
router.get('/', validateGetLoadsQuery, loadController.getAll);
router.get('/active', checkDriver, loadController.getActive);
router.patch('/active/state', checkDriver, loadController.iterateState);
router.get('/:id', validateId, loadController.getOne);
router.put('/:id', checkShipper,
    validateId, validateLoad, loadController.update);
router.delete('/:id', checkShipper, validateId, loadController.delete);
router.post('/:id/post', checkShipper, validateId, loadController.post);
router.get('/:id/shipping_info', checkShipper,
    validateId, loadController.getShippingInfo);

module.exports = router;
