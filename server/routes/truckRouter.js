const express = require('express');
const router = express();
const truckController = require('../controllers/truckController');
const validateTruck = require('../validations/truckValidation');
const validateId = require('../validations/idValidation');

router.post('/', validateTruck, truckController.create);
router.get('/', truckController.getAll);
router.get('/:id', validateId, truckController.getOne);
router.put('/:id', validateId, validateTruck, truckController.update);
router.delete('/:id', validateId, truckController.delete);
router.post('/:id/assign', validateId, truckController.assign);

module.exports = router;
