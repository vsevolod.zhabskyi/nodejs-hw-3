const express = require('express');
const router = express();
const userController = require('../controllers/userController');

router.get('/me', userController.get);
router.patch('/me/password', userController.updatePassword);
router.delete('/me', userController.delete);
router.patch('/me/upload_photo', userController.uploadPhoto);

module.exports = router;
