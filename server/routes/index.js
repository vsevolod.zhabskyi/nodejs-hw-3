const express = require('express');
const router = express();
const authRouter = require('./authRouter');
const userRouter = require('./userRouter');
const truckRouter = require('./truckRouter');
const loadRouter = require('./loadRouter');
const checkAuth = require('../middleware/checkAuthMiddleware');
const checkDriver = require('../middleware/checkDriverRoleMiddleware');

router.use('/auth', authRouter);
router.use('/users', checkAuth, userRouter);
router.use('/trucks', checkAuth, checkDriver, truckRouter);
router.use('/loads', checkAuth, loadRouter);

module.exports = router;
