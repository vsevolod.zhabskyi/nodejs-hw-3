class Log {
  constructor(message) {
    this.message = message;
    this.time = new Date();
  }
}

module.exports = Log;
