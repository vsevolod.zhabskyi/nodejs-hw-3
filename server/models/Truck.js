const {Schema, model} = require('mongoose');

const Truck = new Schema({
  created_by: {type: Schema.Types.ObjectId, ref: 'User', required: true},
  assigned_to: {type: Schema.Types.ObjectId, ref: 'User'},
  type: {type: String, required: true},
  status: {type: String, required: true},
  created_date: {type: Date, required: true},
});

module.exports = model('Truck', Truck);
