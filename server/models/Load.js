const {Schema, model} = require('mongoose');

const Load = new Schema({
  created_by: {type: Schema.Types.ObjectId, ref: 'User', required: true},
  assigned_to: {type: Schema.Types.ObjectId, ref: 'User'},
  status: {type: String, required: true},
  state: {type: String, required: false},
  name: {type: String, required: true},
  pickup_address: {type: String, required: true},
  delivery_address: {type: String, required: true},
  payload: {type: Number, required: true},
  dimensions: {type: Object, required: true},
  logs: {type: Array, required: true},
  created_date: {type: Date, required: true},
});

module.exports = model('Load', Load);
