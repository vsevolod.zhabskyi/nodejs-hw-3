const ApiError = require('../error/ApiError');
const userRole = require('../utils/UserRole');

module.exports = (req, res, next) => {
  try {
    const {role} = req.user;

    if (role !== userRole.DRIVER) {
      return next(ApiError.clientError('This resource is for drivers only'));
    }

    next();
  } catch (e) {
    return next(ApiError.serverError('Server error'));
  }
};
