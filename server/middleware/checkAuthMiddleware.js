const jwt = require('jsonwebtoken');
const userRole = require('../utils/UserRole');
const Truck = require('../models/Truck');
const truckStatus = require('../utils/TruckStatus');
const ApiError = require('../error/ApiError');

module.exports = async (req, res, next) => {
  try {
    const token = req.headers.authorization?.split(' ')[1];
    if (!token) {
      return next(ApiError.clientError('User is not logged in'));
    }

    const {_id, role} = jwt.verify(token, process.env.SECRET_KEY);

    if (role === userRole.DRIVER &&
      (req.method === 'POST' ||
        req.method === 'PUT' ||
        req.method === 'DELETE')) {
      const truck = await Truck.findOne({
        assigned_to: _id,
        status: truckStatus.OL,
      });
      if (truck) {
        return next(ApiError
            .clientError('Driver is not available at the moment'));
      }
    }

    req.user = {_id, role};

    next();
  } catch (e) {
    return next(ApiError.serverError('Server error'));
  }
};
