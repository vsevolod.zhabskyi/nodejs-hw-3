const ApiError = require('../error/ApiError');
const {mongoose} = require('mongoose');

module.exports = (req, res, next) => {
  if (!req.params.id) {
    return next(ApiError.clientError('Id parameter is required'));
  }

  const id = req.params.id;

  try {
    new mongoose.Types.ObjectId(id);
  } catch (e) {
    return next(ApiError.clientError('Id parameter is invalid'));
  }

  next();
};
