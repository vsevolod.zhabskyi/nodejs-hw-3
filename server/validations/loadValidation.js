const Joi = require('joi');
const validate = require('./index');

const schema = Joi.object({
  name: Joi.string().required(),
  payload: Joi.number().not(0).required(),
  pickup_address: Joi.string().required(),
  delivery_address: Joi.string().required(),
  dimensions: Joi.object({
    width: Joi.number().not(0).required(),
    length: Joi.number().not(0).required(),
    height: Joi.number().not(0).required(),
  }).required(),
});

module.exports = validate(schema);
