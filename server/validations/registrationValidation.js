const Joi = require('joi');
const validate = require('./index');
const userRole = require('../utils/UserRole');

const schema = Joi.object({
  email: Joi.string().email({minDomainSegments: 2}),
  password: Joi.string().required(),
  role: Joi.string().required().valid(userRole.DRIVER, userRole.SHIPPER),
});

module.exports = validate(schema);
