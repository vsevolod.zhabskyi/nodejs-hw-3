const Joi = require('joi');
const validate = require('./index');
const truckTypes = require('../utils/TruckTypes');

const schema = Joi.object({
  type: Joi.string().required().valid(...truckTypes.map((t) => t.name)),
});

module.exports = validate(schema);
