const ApiError = require('../error/ApiError');
const Joi = require('joi');
const loadStatus = require('../utils/LoadStatus');

const schema = Joi.object({
  limit: Joi.number().max(50),
  offset: Joi.number(),
  status: Joi.string().valid(...Object.values(loadStatus)),
});

module.exports = (req, res, next) => {
  const {error} = schema.validate(req.query);

  if (error) {
    return next(ApiError.clientError(error.message));
  }

  next();
};
