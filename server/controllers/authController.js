const User = require('../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const ApiError = require('../error/ApiError');

const generateJwt = (_id, role) => {
  return jwt.sign(
      {_id, role},
      process.env.SECRET_KEY,
  );
};

class AuthController {
  async login(req, res, next) {
    try {
      const {email, password} = req.body;

      const user = await User.findOne({email});
      if (!user) {
        return next(ApiError.clientError('No such user'));
      }

      const comparePassword = bcrypt.compareSync(password, user.password);
      if (!comparePassword) {
        return next(ApiError.clientError('Wrong password'));
      }

      const {_id, role} = user;
      const token = generateJwt(_id, role);

      return res.status(200).json({
        jwt_token: token,
      });
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }

  async registration(req, res, next) {
    try {
      const {email, password, role} = req.body;

      const existingUser = await User.findOne({email});
      if (existingUser) {
        return next(ApiError.clientError('This email is already used'));
      }

      const hashPassword = await bcrypt.hash(password, 6);

      await User.create({
        email: email,
        password: hashPassword,
        role: role,
        createdDate: new Date(),
      });

      return res.status(200).json({message: 'Success'});
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }

  async check(req, res, next) {
    try {
      const {user} = req;
      if (!user) {
        return next(ApiError.clientError('User is not logged in'));
      }
      const token = generateJwt(user._id, user.role);

      return res.status(200).json({
        jwt_token: token,
      });
    } catch (e) {
    }
  }
}

module.exports = new AuthController();
