const User = require('../models/User');
const bcrypt = require('bcrypt');
const uuid = require('uuid');
const ApiError = require('../error/ApiError');
const path = require('path');
const fs = require('fs');

class UserController {
  async get(req, res, next) {
    try {
      const {_id} = req.user;

      const user = await User.findOne({_id});
      const {email, role, createdDate, photo} = user;

      res.status(200).json({
        user: {
          _id,
          email,
          role,
          createdDate,
          photo,
        },
      });
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }

  async delete(req, res, next) {
    try {
      const {_id} = req.user;

      await User.deleteOne({_id});

      return res.status(200).json({message: 'Success'});
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }

  async updatePassword(req, res, next) {
    try {
      const {_id} = req.user;
      const {oldPassword, newPassword} = req.body;

      if (!oldPassword || !newPassword) {
        return next(ApiError.clientError('"Old password" and ' +
          '"New password" fields cannot be empty'));
      }

      const user = await User.findOne({_id});

      const comparePassword = bcrypt.compareSync(oldPassword, user.password);
      if (!comparePassword) {
        return next(ApiError.clientError('Wrong password'));
      }

      user.password = await bcrypt.hash(newPassword, 6);

      await user.save();

      return res.status(200).json({message: 'Success'});
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }

  async uploadPhoto(req, res, next) {
    try {
      const img = req.files.img;
      if (!img) {
        return next(ApiError.clientError('Img field required'));
      }

      const {_id} = req.user;

      const filename = uuid.v4() + '.jpg';

      fs.mkdirSync(path.resolve(__dirname, '..', 'static'), {recursive: true});
      await img.mv(path.resolve(__dirname, '..', 'static', filename));

      await User.findOneAndUpdate({_id}, {photo: filename});

      return res.status(200).json({message: 'Photo uploaded successfully'});
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }
}

module.exports = new UserController();
