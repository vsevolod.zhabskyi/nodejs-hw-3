const ApiError = require('../error/ApiError');
const Truck = require('../models/Truck');
const truckStatus = require('../utils/TruckStatus');

class TruckController {
  async create(req, res, next) {
    try {
      const {_id} = req.user;
      const {type} = req.body;

      const truck = {
        created_by: _id,
        assigned_to: null,
        type: type,
        status: truckStatus.IS,
        created_date: new Date(),
      };

      await Truck.create(truck);

      return res.status(200).json({message: 'Success'});
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }

  async update(req, res, next) {
    try {
      const {_id} = req.user;
      const {type} = req.body;
      const truckId = req.params.id;

      const truck = await Truck.findOne({_id: truckId, created_by: _id});
      if (!truck) {
        return next(ApiError.clientError('Truck is not found'));
      }
      if (truck.assigned_to) {
        return next(ApiError.clientError('Forbidden to update assigned truck'));
      }

      truck.type = type;

      await truck.save();


      return res.status(200)
          .json({message: 'Truck details changed successfully'});
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }


  async getAll(req, res, next) {
    try {
      const {_id} = req.user;

      const trucks = await Truck.find({created_by: _id});

      return res.status(200).json({trucks: trucks});
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }

  async getOne(req, res, next) {
    try {
      const {_id} = req.user;
      const truckId = req.params.id;

      const truck = await Truck.findOne({_id: truckId, created_by: _id});
      if (!truck) {
        return next(ApiError.clientError('Truck is not found'));
      }

      return res.status(200).json({truck: truck});
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }

  async assign(req, res, next) {
    try {
      const {_id} = req.user;
      const truckId = req.params.id;

      const trucks = await Truck.find({created_by: _id});

      const assignedTruck = trucks.find((t) => t.assigned_to);
      if (assignedTruck) {
        assignedTruck.assigned_to = null;

        await assignedTruck.save();
      }

      const truck = trucks.find((t) => String(t._id) === truckId);
      if (!truck) {
        return next(ApiError.clientError('Truck is not found'));
      }

      truck.assigned_to = _id;

      await truck.save();

      return res.status(200).json({message: 'Truck assigned successfully'});
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }

  async delete(req, res, next) {
    try {
      const {_id} = req.user;
      const truckId = req.params.id;

      const truck = await Truck.findOne({_id: truckId, created_by: _id});
      if (!truck) {
        return next(ApiError.clientError('Truck is not found'));
      }
      if (truck.assigned_to) {
        return next(ApiError.clientError('Forbidden to delete assigned truck'));
      }

      await truck.remove();

      return res.status(200).json({message: 'Truck deleted successfully'});
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }
}

module.exports = new TruckController();
