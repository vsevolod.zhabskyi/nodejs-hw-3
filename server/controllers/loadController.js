const ApiError = require('../error/ApiError');
const Truck = require('../models/Truck');
const truckStatus = require('../utils/TruckStatus');
const truckTypes = require('../utils/TruckTypes');
const Log = require('../models/Log');
const Load = require('../models/Load');
const loadStatus = require('../utils/LoadStatus');
const loadStates = require('../utils/LoadStates');
const userRole = require('../utils/UserRole');

const getAvailableTruckTypes = ({payload, dimensions}) => {
  const resultArray = [];

  truckTypes.forEach((type) => {
    if (payload > type.payload) {
      return;
    }
    for (const axis in dimensions) {
      if (dimensions[axis] > type.dimensions[axis]) {
        return;
      }
    }
    resultArray.push(type.name);
  });

  return resultArray;
};

class LoadController {
  async create(req, res, next) {
    try {
      const {_id} = req.user;
      const load = req.body;

      await Load.create({
        ...load,
        created_by: _id,
        assigned_to: null,
        status: loadStatus.NEW,
        state: null,
        logs: [new Log('Load has been created')],
        created_date: new Date(),
      });

      return res.status(200).json({message: 'Success'});
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }

  async getAll(req, res, next) {
    try {
      const {_id, role} = req.user;

      const offset = +req.query.offset || 0;
      const limit = +req.query.limit || 10;
      const status = req.query.status;

      const filter = {};

      if (status) {
        filter.status = status;
      }

      switch (role) {
        case userRole.SHIPPER:
          filter.created_by = _id;
          break;
        case userRole.DRIVER:
          filter.assigned_to = _id;
          break;
      }

      const loads = await Load.find(filter)
          .skip(offset).limit(limit);

      return res.status(200).json({loads: loads});
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }

  async getOne(req, res, next) {
    try {
      const {_id} = req.user;

      const loadId = req.params.id;

      const load = await Load.findOne({_id: loadId})
          .or([{assigned_to: _id}, {created_by: _id}]);
      if (!load) {
        return next(ApiError.clientError('Load not found'));
      }

      return res.status(200).json({load: load});
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }

  async getShippingInfo(req, res, next) {
    try {
      const {_id} = req.user;

      const loadId = req.params.id;

      const load = await Load.findOne({_id: loadId, created_by: _id});
      if (!load) {
        return next(ApiError.clientError('Load not found'));
      }

      if (load.status !== loadStatus.ASSIGNED) {
        return next(ApiError.clientError('Load is not active'));
      }

      const truck = await Truck.findOne({assigned_to: load.assigned_to});

      return res.status(200).json({
        load: load,
        truck: truck,
      });
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }

  async update(req, res, next) {
    try {
      const {_id} = req.user;
      const updatedData = req.body;
      const loadId = req.params.id;

      const load = await Load.findOne({_id: loadId, created_by: _id});
      if (!load) {
        return next(ApiError.clientError('Load is not found'));
      }
      if (load.status !== loadStatus.NEW) {
        return next(ApiError.clientError(`Forbidden to update load, ` +
          `that is already ${load.status.toLowerCase()}`));
      }

      for (const prop in updatedData) {
        if (updatedData.hasOwnProperty(prop)) {
          load[prop] = updatedData[prop];
        }
      }

      await load.save();

      return res.status(200)
          .json({message: 'Load details changed successfully'});
    } catch (e) {
      console.log(e);
      return next(ApiError.serverError('Server error'));
    }
  }

  async post(req, res, next) {
    try {
      const {_id} = req.user;
      const loadId = req.params.id;

      const load = await Load.findOne({_id: loadId, created_by: _id});
      if (!load) {
        return next(ApiError.clientError('Load is not found'));
      }
      if (load.status !== loadStatus.NEW) {
        return next(ApiError.clientError(`Cannot post load, that is already` +
          ` ${load.status.toLowerCase()}`));
      }

      load.status = loadStatus.POSTED;
      load.logs.push(new Log(`Load status changed to 'POSTED'`));

      await load.save();

      const availableTruckTypes = getAvailableTruckTypes({
        payload: load.payload,
        dimensions: load.dimensions,
      });

      const truck = await Truck.findOne()
          .where('status').equals(truckStatus.IS)
          .where('assigned_to').ne(null)
          .where('type').in(availableTruckTypes);

      if (!truck) {
        load.status = loadStatus.NEW;
        load.logs.push(new Log(`Unable to find available truck`));
        load.logs.push(new Log(`Load status changed back to 'NEW'`));

        await load.save();

        return res.status(200).json({
          message: `Unable to find available truck`,
          driver_found: false,
        });
      }

      load.status = loadStatus.ASSIGNED;
      load.logs.push(new Log(`Load status changed to 'ASSIGNED'`));

      load.assigned_to = truck.created_by;
      load.logs.push(new Log(`Load assigned to driver with` +
        ` id ${load.assigned_to}`));

      load.state = loadStates[0].name;
      load.logs.push(new Log(`Load state is ${load.state}`));

      await load.save();

      truck.status = truckStatus.OL;
      await truck.save();

      return res.status(200).json({
        message: 'Load posted successfully',
        driver_found: true,
      });
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }

  async delete(req, res, next) {
    try {
      const {_id} = req.user;
      const loadId = req.params.id;

      const load = await Load.findOne({_id: loadId, created_by: _id});
      if (!load) {
        return next(ApiError.clientError('Load is not found'));
      }
      if (load.status !== loadStatus.NEW) {
        return next(ApiError.clientError(`Forbidden to delete load, ` +
          `that is already ${load.status.toLowerCase()}`));
      }

      await load.remove();

      return res.status(200).json({message: 'Load deleted successfully'});
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }

  async getActive(req, res, next) {
    try {
      const {_id} = req.user;

      const load = await Load.findOne({
        assigned_to: _id,
        status: loadStatus.ASSIGNED,
      });
      if (!load) {
        return next(ApiError.clientError('Load is not found'));
      }

      return res.status(200).json({load: load});
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }

  async iterateState(req, res, next) {
    try {
      const {_id} = req.user;

      const load = await Load.findOne({
        assigned_to: _id,
        status: loadStatus.ASSIGNED,
      });
      if (!load) {
        return next(ApiError.clientError('Load is not found'));
      }

      const stateIndex = loadStates.find((s) => s.name === load.state).index;
      const newIndex = stateIndex + 1;
      const newState = loadStates.find((s) => s.index === newIndex).name;

      load.state = newState;
      load.logs.push(new Log(`Load state changed to '${newState}'`));

      if (newIndex >= loadStates.length - 1) {
        load.status = loadStatus.SHIPPED;
        load.logs.push(new Log(`Load status changed to '${load.status}'`));

        await Truck.findOneAndUpdate(
            {assigned_to: load.assigned_to},
            {status: truckStatus.IS},
        );
      }

      await load.save();

      return res.status(200)
          .json({message: `Load state changed to '${newState}'`});
    } catch (e) {
      return next(ApiError.serverError('Server error'));
    }
  }
}

module.exports = new LoadController();
